/* Copyright (C) 2020 Stephan Kreutzer
 *
 * This file is part of Time Tracker.
 *
 * Time Tracker is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * Time Tracker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Time Tracker. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let timeTracker = null;
let globalTime = 0;

function initialize()
{
    timeTracker = new TimeTracker();

    document.getElementById("participant_form").onsubmit = function (event) {
        event.preventDefault();
        timeTracker.addParticipant();
    }

    return 0;
}

function TimeTracker()
{
    let self = this;

    let _participants = [];
    let _time = 0;

    {
        let target = document.getElementById("participants_body");

        if (target != null)
        {
            let participantId = _participants.length + 1;

            let row = document.createElement("tr");

            let cell1 = document.createElement("td");

            let button = document.createElement("input");
            button.setAttribute("id", "timer-button-" + participantId);
            button.setAttribute("type", "button");
            button.setAttribute("value", "rec");
            button.setAttribute("onclick", "timeTracker.trackParticipant(" + participantId + ");");

            cell1.appendChild(button);

            let cell2 = document.createElement("td");

            let participantName = document.createTextNode("silence");
            cell2.appendChild(participantName);

            let cell3 = document.createElement("td");
            cell3.setAttribute("id", "timer-" + participantId);
            cell3.setAttribute("class", "td-right");

            let cell4 = document.createElement("td");
            cell4.setAttribute("id", "time-total-" + participantId);
            cell4.setAttribute("class", "td-right");

            row.appendChild(cell1);
            row.appendChild(cell2);
            row.appendChild(cell3);
            row.appendChild(cell4);

            target.appendChild(row);
            _participants.push(new Timer(participantId, "silence"));
        }
        else
        {
            console.log("Can't find element \"participants_body\".");
            return -1;
        }
    }

    self.addParticipant = function()
    {
        let participantField = document.getElementById("participant_name");

        if (participantField == null)
        {
            console.log("Can't find element \"participant_name\".");
            return -1;
        }

        if (participantField.value.length <= 0)
        {
            return 0;
        }

        let target = document.getElementById("participants_body");

        if (target == null)
        {
            console.log("Can't find element \"participants_body\".");
            return -1;
        }


        let participantId = _participants.length + 1;

        let row = document.createElement("tr");

        let cell1 = document.createElement("td");

        let button = document.createElement("input");
        button.setAttribute("id", "timer-button-" + participantId);
        button.setAttribute("type", "button");
        button.setAttribute("value", "rec");
        button.setAttribute("onclick", "timeTracker.trackParticipant(" + participantId + ");");

        cell1.appendChild(button);

        let cell2 = document.createElement("td");

        let participantName = document.createTextNode(participantField.value);
        cell2.appendChild(participantName);

        let cell3 = document.createElement("td");
        cell3.setAttribute("id", "timer-" + participantId);
        cell3.setAttribute("class", "td-right");

        let cell4 = document.createElement("td");
        cell4.setAttribute("id", "time-total-" + participantId);
        cell4.setAttribute("class", "td-right");

        row.appendChild(cell1);
        row.appendChild(cell2);
        row.appendChild(cell3);
        row.appendChild(cell4);

        target.insertBefore(row, target.firstChild);
        _participants.push(new Timer(participantId, participantField.value));

        {
            let participantLog = document.getElementById("participant_log");

            if (participantLog != null)
            {
                let text = document.createTextNode("\"" + (new Date().toISOString()) + "\",\"" + secondsToTimeString(globalTime) + "\",\"" + participantField.value.replace("\"", "\"\"") + "\",\"joined\"");

                participantLog.appendChild(text);
                participantLog.appendChild(document.createElement("br"));
            }
            else
            {
                console.log("Can't find element \"participant_log\".");
            }
        }

        participantField.value = "";

        return 0;
    }

    self.trackParticipant = function(id)
    {
        let target = document.getElementById("timer-" + id);

        if (target == null)
        {
            console.log("Can't find element \"timer-" + id + "\".");
            return -1;
        }


        for (let i = 0, max = _participants.length; i < max; i++)
        {
            let participant = _participants[i];
            let participantId = participant.getId();
            let button = document.getElementById("timer-button-" + participantId);

            if (button == null)
            {
                console.log("Can't find element \"timer-button-" + participantId + "\".");
                continue;
            }

            if (participantId == id)
            {
                participant.setActive(true);
                button.disabled = true;
            }
            else
            {
                let wasActive = participant.getActive();

                participant.setActive(false);
                button.disabled = false;

                if (wasActive == true)
                {
                    let timeTotal = document.getElementById("time-total-" + participantId);

                    if (timeTotal != null)
                    {
                        timeTotal.innerText = secondsToTimeString(participant.getTime());
                    }
                    else
                    {
                        console.log("Can't find element \"time-total-" + participantId + "\".");
                    }
                }
            }
        }

        return 0;
    }
}

function Timer(id, name)
{
    let self = this;

    let _id = id;
    let _name = name;
    let _timer = null;
    let _time = 0;

    self.getId = function()
    {
        return _id;
    }

    self.getTime = function()
    {
        return _time;
    }

    self.getActive = function()
    {
        return _timer != null;
    }

    self.setActive = function(active)
    {
        if (active == true)
        {
            if (_timer != null)
            {
                return 0;
            }

            {
                let trackLog = document.getElementById("track_log");

                if (trackLog != null)
                {
                    let text = document.createTextNode("\"" + (new Date().toISOString()) + "\",\"" + secondsToTimeString(globalTime + 1) + "\",\"" + _name.replace("\"", "\"\"") + "\",\"speaking\"");

                    trackLog.appendChild(text);
                    trackLog.appendChild(document.createElement("br"));
                }
                else
                {
                    console.log("Can't find element \"track_log\".");
                }
            }

            _timer = setInterval(_tick, 1000);
        }
        else
        {
            if (_timer == null)
            {
                return 0;
            }

            clearInterval(_timer);
            _timer = null;
        }

        return 0;
    }

    let _tick = function()
    {
        _time += 1;
        globalTime += 1;

        let target = document.getElementById("timer-" + _id);

        if (target == null)
        {
            console.log("Can't find element \"timer-" + id + "\".");
            clearInterval(_timer);
            _timer = null;
            return -1;
        }

        target.innerText = _time;

        return 0;
    }
}

function secondsToTimeString(seconds)
{
    let h = Math.floor(seconds / 3600);
    let m = Math.floor(seconds % 3600 / 60);
    let s = Math.floor(seconds % 3600 % 60);

    let string = "";

    if (s <= 9)
    {
        string += "0";
    }

    string = m + ":" + string + s;

    if (h > 0)
    {
        string = h + ":" + string;
    }

    return string;
}
